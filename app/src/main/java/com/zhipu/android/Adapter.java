package com.zhipu.android;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.zhipu.android.entity.AppInfo;

import java.util.List;

public class Adapter extends ArrayAdapter<AppInfo> {

    private Context mContext;
    private int resource;

    public Adapter(Context context, int resource, List<AppInfo> data) {
        super(context, resource, data);
        this.mContext = context;
        this.resource = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(resource, null);
            holder.image = convertView.findViewById(R.id.icon);
            holder.title = convertView.findViewById(R.id.title);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        AppInfo appInfo = getItem(position);
        holder.image.setImageDrawable(appInfo.appIcon);
        holder.title.setText(appInfo.appName);

        return convertView;
    }

    final class ViewHolder {
        ImageView image;
        TextView title;
    }

}
