package com.zhipu.android.helper;

import com.zhipu.android.MainApplication;
import com.zhipu.android.R;

/**
 * @author 樊夏阳
 * @file FileHelper.java
 * @function 文件操作相关工具类
 * @date 2015-8-12
 */
public class FileHelper {

    /*
    * @function    根据文件名，计算文件后缀
    * @fileName    文件名
    * */
    public static String getFileExt(String fileName){
        return fileName.substring(fileName.lastIndexOf('.') + 1);
    }

    /*
    * @function    根据文件后缀，计算文件类型索引
    * @fileExt     文件名后缀
    * */
    public static int getFileTypeIndex(String fileExt){
        final String fileExtArray[] = MainApplication.application().getResources().getStringArray(R.array.file_ext_array);
        int fileTypeIndex = -1;
        for (int i = 0; i < fileExtArray.length; i++){
            if (fileExtArray[i].equals(fileExt)){
                fileTypeIndex = i;
            }
        }
        return fileTypeIndex;
    }

    /*
    * @function    根据文件类型索引，计算文件类型名
    * @fileExt     文件类型索引
    * */
    public static String getFileTypeName(int fileTypeIndex){
        String fileTypeArray[] = MainApplication.application().getResources().getStringArray(R.array.file_type_array);
        return fileTypeArray[fileTypeIndex];
    }

}
