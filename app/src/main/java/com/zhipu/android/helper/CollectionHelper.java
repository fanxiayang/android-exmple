package com.zhipu.android.helper;

import java.util.List;
import java.util.Set;

/**
 * @author 樊夏阳
 * @file CollectionHelper.java
 * @function 集合操作相关工具类
 * @date 2015-8-12
 */
public class CollectionHelper {
    /*
	* @function		判断两个集合是否一致
	* */
    public static boolean compareTo(List<?> first,List<?> second) {
        if (first == second){
            return true;
        }
        if (first == null && second != null){
            return false;
        }
        if (first != null && second == null){
            return false;
        }
        if (first.size() != second.size()){
            return false;
        }
        for (int i = 0; i < first.size(); i++){
            if (!first.get(i).equals(second.get(i))){
                return false;
            }
        }
        return true;
    }

    /*
    * @function     判断指定集合不空且长度大于0
    * @list         目标集合
    * */
    public static boolean haveData(List<?> list){
        if (list == null){
            return false;
        }
        if (list.size() <= 0){
            return false;
        }
        return true;
    }

    /*
    * @function     判断指定集合不空且长度大于0
    * @list         目标集合
    * */
    public static boolean haveData(Set<?> set){
        if (set == null){
            return false;
        }
        if (set.size() < 0){
            return false;
        }
        return true;
    }
}
