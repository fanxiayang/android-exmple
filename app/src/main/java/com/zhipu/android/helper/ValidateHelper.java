package com.zhipu.android.helper;

import java.util.regex.Pattern;

/**
 * @author 樊夏阳
 * @file ValidateHelper.java
 * @function 合法校验相关工具类
 * @date 2015/8/6
 * @copyright   Copyright(c) 2015 www.sjgtw.com All rights reserved.
 */
public class ValidateHelper {

    /*
	* @function		校验字符串是否为空串或为"[]"
	* */
    public static boolean empty(String str) {
        return str == null || str.trim().length() == 0 || str.equals("[]");
    }

    /*
	* @function		校验字符串是否为合法邮箱地址
	* */
    public static boolean validateEmail(String str) {
        if (empty(str)) {
            return false;
        }
        boolean matched = Pattern
                .matches(
                        "[a-zA-Z_\\.]{1,}[0-9]{0,}@(([a-zA-z0-9]-*){1,}\\.){1,3}[a-zA-z\\-]{1,}",
                        str);
        return matched;
    }

    /*
	* @function		校验字符串是否为合法手机号码
	* */
    public static boolean validateMobile(String str) {
        if (empty(str)) {
            return false;
        }
        boolean matched = Pattern.matches("1\\d{10}", str);
        return matched;
    }
}
