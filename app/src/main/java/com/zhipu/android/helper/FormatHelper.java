package com.zhipu.android.helper;

import java.text.DecimalFormat;

/**
 * @author 樊夏阳
 * @file FormatHelper.java
 * @function 数据格式化操作
 * @date 2015/9/15
 */
public class FormatHelper {

    /*
    * @function     保留两位有效数字
    * */
    public static String formatDouble(Double val){
        DecimalFormat decimalFormat = new DecimalFormat("#,##0.00");
        if (val == 0){
            return "0";
        }
        return decimalFormat.format(val);
    }
}
