package com.zhipu.android.helper;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

/**
 * @author 樊夏阳
 * @file ImageHelper.java
 * @function 图片操作相关工具类
 * @date 2015/9/17
 */
public class ImageHelper {
    /*
    * @function     图库中的Uri转换为File路径
    * */
    public static String getRealFilePath( final Context context, final Uri uri ) {
        String data = null;

        if (null == uri){
            return null;
        }

        final String scheme = uri.getScheme();
        if (scheme == null){
            data = uri.getPath();
        }
        else if(ContentResolver.SCHEME_FILE.equals(scheme)){
            data = uri.getPath();
        } else if(ContentResolver.SCHEME_CONTENT.equals(scheme)){
            Cursor cursor = context.getContentResolver().query( uri, new String[] { MediaStore.Images.ImageColumns.DATA }, null, null, null );
            if ( null != cursor ) {
                if ( cursor.moveToFirst() ) {
                    int index = cursor.getColumnIndex( MediaStore.Images.ImageColumns.DATA );
                    if ( index > -1 ) {
                        data = cursor.getString( index );
                    }
                }
                cursor.close();
            }
        }

        return data;
    }
}
