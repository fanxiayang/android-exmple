package com.zhipu.android.helper;

import android.util.Base64;

/**
 * @file        Base64Helper.java
 * @function    Base64加解密算法
 * @author      樊夏阳
 * @date        2015-11-26
 */
public class Base64Helper {

    /*
    * @function     加密算法
    * */
    public static String encode( byte[] plainText){
        String encodeText = new String(Base64.encode(plainText,Base64.NO_WRAP));
        encodeText = encodeText.replace("+","-");
        encodeText = encodeText.replace("/","_");
        return encodeText;
    }

    /*
    * @function     解密算法
    * */
    public static byte[] decode( String encodeText){
        encodeText = encodeText.replace("-","+");
        encodeText = encodeText.replace("_","/");
        return Base64.decode(encodeText,Base64.NO_WRAP);
    }

}
