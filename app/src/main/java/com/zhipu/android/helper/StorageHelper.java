package com.zhipu.android.helper;

import android.content.Context;
import android.os.Environment;

import com.zhipu.android.MainApplication;

import java.io.File;
import java.io.IOException;

/**
 * @function    文件系统工具类
 * @author      樊夏阳
 * @date        2015-11-16
 * @copyright   Copyright(c) 2015 www.sjgtw.com All rights reserved.
 */
public final class StorageHelper {

	/*
	* @function		获取应用相关缓存目录，如果存在外存，则为外部缓存；否则，为内部缓存；
	* */
	public static File getCacheDirectory() {

		File appCacheDir;

		if (existExternalStorage()) {
			appCacheDir = getExternalCacheDirectory();
		}else {
			appCacheDir = getInnerCacheDirectory();
		}

		return appCacheDir;
	}

	/*
	* @function		获取内部存储缓存目录
	* */
	public static File getInnerCacheDirectory() {
		File appCacheDir;

		Context context = MainApplication.application();
		appCacheDir = context.getCacheDir();

		return appCacheDir;
	}

	/*
	* @function		获取外部存储缓存目录
	* */
	public static File getExternalCacheDirectory() {
		Context context = MainApplication.application();

		File dataDir = new File(new File(Environment.getExternalStorageDirectory(), "Android"), "data");
		File appCacheDir = new File(new File(dataDir, context.getPackageName()), "cache");
		if (!appCacheDir.exists()) {
			if (!appCacheDir.mkdirs()) {
				LogHelper.w("Unable to create external cache directory");
				return null;
			}
			try {
				new File(appCacheDir, ".nomedia").createNewFile();
			} catch (IOException e) {
				LogHelper.i("Can't create \".nomedia\" file in application external cache directory");
			}
		}
		return appCacheDir;
	}

	/*
	* @function		判断内存卡是否存在
	* */
	public static boolean existExternalStorage() {
		if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
			return true;
		}else {
			return false;
		}
	}
}
