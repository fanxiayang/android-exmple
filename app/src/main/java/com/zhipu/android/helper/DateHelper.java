package com.sjgtw.web.util;

import com.zhipu.android.helper.StringHelper;

import java.util.Date;

/**
 * @author 樊夏阳
 * @file DateHelper.java
 * @function 日期操作相关工具类
 * @date 2015/9/17
 */
public class DateHelper {
    /*
    * @function     获取当前系统时间
    * */
    public static String getNow() {
        String nowTime = "";
        Date date = new Date();
        nowTime = StringHelper.fromDefaultFormatDate(date);
        return nowTime;
    }
}
