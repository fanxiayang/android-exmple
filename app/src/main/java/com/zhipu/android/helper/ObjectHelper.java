package com.zhipu.android.helper;

/**
 * @author 樊夏阳
 * @file ObjectHelper.java
 * @function 对象操作相关工具类
 * @date 2015-8-12
 */
public class ObjectHelper {
    /*
	* @function		判断两个对象是否一致
	* */
    public static boolean compareTo(Object first,Object second) {
        if (first == second){
            return true;
        }
        if (first == null && second != null){
            return false;
        }
        if (first != null && second == null){
            return false;
        }
        return first.equals(second);
    }

    /*
    * @function     如果指定对象为空，则创建对象
    * @object       目标对象
    * @clazz        目标类.class
    * */
    public static <T> T fill(T object,Class<T> clazz){
        T rObject = object;

        if (empty(rObject)){
            rObject = ReflectHelper.createInstance(clazz);
        }

        return rObject;
    }

    /*
	* @function		判断对象是否为空
	* */
    public static boolean empty(Object object) {
        return object == null;
    }

}
