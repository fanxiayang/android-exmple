package com.zhipu.android.activity;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.zhipu.android.Adapter;
import com.zhipu.android.R;
import com.zhipu.android.entity.AppInfo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends Activity {

    private List<String> blackPackages = Arrays.asList("com.zhipu.apps","com.koushikdutta.vysor");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final List<AppInfo> appList = new ArrayList<>();
        List<PackageInfo> installedPackages = getPackageManager().getInstalledPackages(0);

        for (PackageInfo installedPackage : installedPackages) {
            if ((installedPackage.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) == 0) {
                if (!blackPackages.contains(installedPackage.packageName)) {
                    AppInfo appInfo = new AppInfo();
                    appInfo.appName = installedPackage.applicationInfo.loadLabel(getPackageManager()).toString();
                    appInfo.appIcon = installedPackage.applicationInfo.loadIcon(getPackageManager());
                    appInfo.packageName = installedPackage.packageName;
                    appList.add(appInfo);
                }
            }
        }

        ListView listView = findViewById(R.id.list);
        Adapter adapter = new Adapter(this, R.layout.list_item, appList);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener((adapterView, view, i, l) -> {
            AppInfo ai = appList.get(i);
            Intent resolveIntent = new Intent(Intent.ACTION_MAIN, null);
            resolveIntent.addCategory(Intent.CATEGORY_LAUNCHER);
            resolveIntent.setPackage(ai.packageName);
            List<ResolveInfo> intentActivities = getPackageManager().queryIntentActivities(resolveIntent, 0);
            Log.i("size", intentActivities.size() + "");
            ResolveInfo mainActivity = intentActivities.iterator().next();
            if (mainActivity != null) {
                String packageName = mainActivity.activityInfo.packageName;
                String className = mainActivity.activityInfo.name;
                Log.i("MainActivity", className);
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_LAUNCHER);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ComponentName cn = new ComponentName(packageName, className);
                intent.setComponent(cn);
                startActivity(intent);
            }
        });

    }

}
