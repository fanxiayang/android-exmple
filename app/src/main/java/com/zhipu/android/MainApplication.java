package com.zhipu.android;

import android.annotation.TargetApi;
import android.app.Application;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;

import com.aquery.AQuery;

public class MainApplication extends Application {

	private static MainApplication application;
	private static AQuery mainAQuery;

	private int versionCode = 0;

	public int getVersionCode() {
		if (this.versionCode <= 0 ) {
			try {
				PackageManager packageManager = getPackageManager();
				PackageInfo packInfo = packageManager.getPackageInfo(getPackageName(), 0);
				this.versionCode = packInfo.versionCode;
			} catch (Exception e) {

			}
		}
		return this.versionCode;
	}

	/*
	* @function	获取全局应用对象
	* @date		2015-7-30
	* */
	public static MainApplication application() {
		return application;
	}

	/*
	* @function	获取应用级别的AQuery对象
	* @date		2015-7-30
	* */
	public static AQuery getMainAQuery() {
		if (mainAQuery == null){
			mainAQuery = new AQuery(application());
		}
		return mainAQuery;
	}

	@TargetApi(Build.VERSION_CODES.GINGERBREAD)
	@Override
	public void onCreate() {
		super.onCreate();
		application = this;
	}

	@Override
	public void onTerminate() {
		super.onTerminate();
	}

	@Override
	public void onLowMemory() {
		super.onLowMemory();
	}

}
