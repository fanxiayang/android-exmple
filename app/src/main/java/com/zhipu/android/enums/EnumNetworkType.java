package com.zhipu.android.enums;

/**
 * @author 樊夏阳
 * @file EnumNetworkType.java
 * @function 枚举网络类型
 * @date 2015/8/6
 */
public enum EnumNetworkType {
    NetworkTypeNone,                //无连接
    NetworkTypeMobile,              //数据连接
    NetworkTypeWifi,                //wifi连接
}
