package com.zhipu.android.entity;

import android.graphics.drawable.Drawable;

public class AppInfo {
    public String appName = "";
    public String packageName = "";
    public Drawable appIcon = null;
}
